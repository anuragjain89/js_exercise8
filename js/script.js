var Box = function (domId) {
  this.domId = domId;
  this.list = '';
};

Box.prototype.addToBox = function (element) {
  this.list.appendChild(element);
};

Box.prototype.init = function (initialListArray) {
  this.list = document.getElementById(this.domId);
  this.list.appendChild(this.domElementsFragment(initialListArray));
};

Box.prototype.domElementsFragment = function (initialListArray) {
  var docFrag = document.createDocumentFragment();
  initialListArray.forEach(function (elementName) {
    docFrag.appendChild(this.createDOMElement(elementName));
  }.bind(this));
  return docFrag;
};

Box.prototype.createDOMElement = function (elementName) {
  var option = document.createElement('option');
  option.id = elementName;
  var text = document.createTextNode(elementName);
  option.appendChild(text);
  return option;
};

var ItemMoveButton = function (fromBox, toBox, domElement) {
  this.fromBox = fromBox;
  this.toBox = toBox;
  this.domElement = domElement;
};

var getCallbackForElement = function (fromBox, toBox, element) {
  var button = new ItemMoveButton(fromBox, toBox, element);
  return function () {
    var selector = '#' + button.fromBox.domId + ' option:checked';
    var ElementsToBeMoved = Array.prototype.slice.call(document.querySelectorAll(selector));
    ElementsToBeMoved.forEach(function (element) {
      button.toBox.addToBox(element);
    });
  };
};

var left = new Box('left');
left.init(['Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor']);

var right = new Box('right');
right.init([]);

var toRight = document.getElementById('add');
var toLeft = document.getElementById('remove');

toRight.addEventListener('click', getCallbackForElement(left, right, toRight));
toLeft.addEventListener('click', getCallbackForElement(right, left, toLeft));
